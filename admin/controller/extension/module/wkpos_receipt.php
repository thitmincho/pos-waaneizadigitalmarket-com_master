<?php
class ControllerExtensionModuleWkposReceipt extends Controller {
	private $error = array();

  public function install() {

    $this->load->model('wkpos/wkpos_receipt');
		$this->load->model('setting/event');
		$this->model_wkpos_wkpos_receipt->createTable();
    $this->model_setting_event->addEvent('wkpos_receipt','admin/view/common/column_left/before','extension/module/wkpos_receipt/addSubmenu');

	}

	public function uninstall() {

		$this->load->model('setting/event');
		$this->load->model('wkpos/wkpos_receipt');
		$this->model_wkpos_wkpos_receipt->deleteTable();
		$this->model_setting_event->deleteEventByCode('wkpos_receipt');

	}

	public function index() {
		$this->load->language('extension/module/wkpos_receipt');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_wkpos_receipt', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/wkpos_receipt', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/wkpos_receipt', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_wkpos_receipt_status'])) {
			$data['module_wkpos_receipt_status'] = $this->request->post['module_wkpos_receipt_status'];
		} else {
			$data['module_wkpos_receipt_status'] = $this->config->get('module_wkpos_receipt_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/wkpos_receipt', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/wkpos_receipt')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

  public function addSubmenu(&$route = false, &$data = false, &$output = false) {

    $this->load->language('extension/module/wkpos_receipt');
    if ($this->user->hasPermission('access', 'wkpos/wkpos_receipt') && $this->config->get('module_wkpos_receipt_status')) {
			$pos_receipt = array(
			 'name'	    => $this->language->get('text_receipt'),
			 'href'     => $this->url->link('wkpos/wkpos_receipt', 'user_token=' . $this->session->data['user_token'], true),
			 'children' => array()
			);
	    foreach($data['menus'] as $key => $menu) {
	      if($menu['id'] == 'menu-pos') {
	        foreach($menu['children'] as $ch_key => $submenu) {
	          if($submenu['name'] == 'Users') {
	            array_push($data['menus'][$key]['children'] , $pos_receipt);
	          }
	        }
	      }
	    }
    }
  }
}
