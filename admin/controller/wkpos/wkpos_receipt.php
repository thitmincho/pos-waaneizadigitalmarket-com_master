<?php
class ControllerWkposWkposreceipt extends Controller {
  private $error = array();

  public function index() {

    if(!$this->config->get('module_wkpos_receipt_status') || !$this->config->get('module_wkpos_status')) {
      $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
    }

    $this->load->model('wkpos/wkpos_receipt');
    $this->load->model('tool/image');
    $this->getList();
  }

  public function getList() {

    $data = array();
    $data = array_merge($data, $this->load->language('wkpos/wkpos_receipt'));
    $this->document->setTitle($data['heading_title']);

    if(isset($this->request->get['filter_status'])) {
      $filter_status = $this->request->get['filter_status'];
    } else {
      $filter_status = '';
    }

    if(isset($this->request->get['filter_name'])) {
      $filter_name = $this->request->get['filter_name'];
    } else {
      $filter_name = '';
    }

    if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'receipt_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

    $url = '';
    if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
     'text' => $this->language->get('text_home'),
     'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
    );

    $data['breadcrumbs'][] = array(
     'text' => $data['heading_title'],
     'href' => $this->url->link('wkpos/wkpos_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true)
    );

    $data['add'] = $this->url->link('wkpos/wkpos_receipt/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('wkpos/wkpos_receipt/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
    $data['user_token'] = $this->session->data['user_token'];

    $filter_data = array(
     'filter_name'   => $filter_name,
     'filter_status' => $filter_status,
     'sort'          => $sort,
     'order'         => $order,
     'start'         => ($page - 1) * $this->config->get('config_limit_admin'),
     'limit'         => $this->config->get('config_limit_admin')
    );

    $data['receipts'] = array();

    $results = $this->model_wkpos_wkpos_receipt->getReceipts($filter_data);
    $total = $this->model_wkpos_wkpos_receipt->getTotalReceipts($filter_data);

    foreach ($results as $result) {

			if (is_file(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 40, 40);
			}
      $outlets = array();
      $ids = $this->model_wkpos_wkpos_receipt->getOutletsByReceiptID($result['receipt_id']);

  		if($ids) {
        foreach ($ids as $id) {
    			$outlet_info = $this->model_wkpos_wkpos_receipt->getOutlet($id);
    			if ($outlet_info) {
    				$outlets[] = array(
    					'outlet_id' => $outlet_info['outlet_id'],
    					'name'      => $outlet_info['name']
    				);
    			}
    		}
      }

      $data['receipts'][] = array(
       'receipt_id' => $result['receipt_id'],
       'name'       => $result['receipt_name'],
       'image'      => $image,
       'outlets'    => $outlets,
       'status'     => $result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
       'edit'       => $this->url->link('wkpos/wkpos_receipt/edit', 'user_token=' . $this->session->data['user_token'] . '&receipt_id=' . $result['receipt_id'] . $url, true),
       'preview'    => $this->url->link('wkpos/wkpos_receipt/printPreview', 'user_token=' . $this->session->data['user_token'] . '&receipt_id=' . $result['receipt_id'] , true)
      );

    }

    if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

    if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

    $url = '';
    if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

    if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

    $data['sort_name'] = $this->url->link('wkpos/wkpos_receipt', 'user_token=' . $this->session->data['user_token'] . '&sort=receipt_name' . $url, true);

		$data['sort_status'] = $this->url->link('wkpos/wkpos_receipt', 'user_token=' . $this->session->data['user_token'] . '&sort=status' . $url, true);

    $url = '';
    if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

    if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

    $pagination = new Pagination();
		$pagination->total = $total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('wkpos/wkpos_receipt', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total - $this->config->get('config_limit_admin'))) ? $total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total, ceil($total / $this->config->get('config_limit_admin')));

    $data['filter_name'] = $filter_name;
		$data['filter_status'] = $filter_status;
		$data['sort'] = $sort;
		$data['order'] = $order;

    $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('wkpos/wkpos_receipt', $data));
  }

  public function edit() {
    $this->load->language('wkpos/wkpos_receipt');
    $this->load->model('wkpos/wkpos_receipt');
    $this->load->model('tool/image');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

      $this->model_wkpos_wkpos_receipt->editReceipt($this->request->get['receipt_id'],$this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

      $url = $this->getUrl();

      $this->response->redirect($this->url->link('wkpos/wkpos_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true));
    }

    $this->getForm();
  }

  public function add() {

    $this->load->language('wkpos/wkpos_receipt');
    $this->load->model('wkpos/wkpos_receipt');
    $this->load->model('tool/image');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

      $this->model_wkpos_wkpos_receipt->addReceipt($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

      $url = $this->getUrl();

      $this->response->redirect($this->url->link('wkpos/wkpos_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true));
    }

    $this->getForm();
  }

  public function getUrl() {
    $url = '';
    if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

    if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

    return $url;
  }

  public function getForm() {

    if(!$this->config->get('module_wkpos_receipt_status') || !$this->config->get('module_wkpos_status')) {
      $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
    }

    $data = array();
    $data = array_merge($data, $this->load->language('wkpos/wkpos_receipt'));
    $this->document->setTitle($data['heading_title']);

    $url = $this->getUrl();

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
     'text' => $this->language->get('text_home'),
     'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
    );

    $data['breadcrumbs'][] = array(
     'text' => $data['heading_title'],
     'href' => $this->url->link('wkpos/wkpos_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true)
    );

    if(isset($this->request->get['receipt_id'])) {

      $data['breadcrumbs'][] = array(
       'text' => $data['text_edit'],
       'href' => $this->url->link('wkpos/wkpos_receipt/edit', 'user_token=' . $this->session->data['user_token'] . '&receipt_id=' . $this->request->get['receipt_id'] . $url, true)
      );
      $data['action'] = $this->url->link('wkpos/wkpos_receipt/edit', 'user_token=' . $this->session->data['user_token'] . '&receipt_id=' . $this->request->get['receipt_id'] . $url, true);

    } else {

      $data['breadcrumbs'][] = array(
       'text' => $data['text_add'],
       'href' => $this->url->link('wkpos/wkpos_receipt/add', 'user_token=' . $this->session->data['user_token'] . $url, true)
      );
      $data['action'] = $this->url->link('wkpos/wkpos_receipt/add', 'user_token=' . $this->session->data['user_token'] . $url, true);

    }

    $data['cancel'] = $this->url->link('wkpos/wkpos_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true);
    $data['user_token'] = $this->session->data['user_token'];

    $config_array = array(
     'receipt_name',
     'print_size',
     'print_font_weight',
     'store_name',
     'store_address',
     'order_date',
     'order_time',
     'order_id',
     'order_note',
     'cashier_name',
     'customer_name',
     'shipping_mode',
     'payment_mode',
     'store_logo',
     'image',
     'extra_information',
     'show_content_header',
     'content_header',
     'show_content_footer',
     'content_footer',
     'status',
    );
    if(isset($this->request->get['receipt_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      $receipt = $this->model_wkpos_wkpos_receipt->getReceipt($this->request->get['receipt_id']);
    } else {
      $receipt = array();
    }

    foreach($config_array as $value) {
      if(isset($this->request->post[$value])) {
        $data[$value] = $this->request->post[$value];
      } else if(isset($receipt[$value])) {
        $data[$value] = $receipt[$value];
      } else {
        $data[$value] = '';
      }
    }

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($receipt) && is_file(DIR_IMAGE . $receipt['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($receipt['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

    if (isset($this->request->post['outlets'])) {
			$outlets = $this->request->post['outlets'];
		} elseif (isset($this->request->get['receipt_id'])) {
			$outlets = $this->model_wkpos_wkpos_receipt->getOutletsByReceiptID($this->request->get['receipt_id']);
		} else {
			$outlets = array();
		}

		$data['outlets'] = array();

    foreach ($outlets as $outlet) {
			$outlet_info = $this->model_wkpos_wkpos_receipt->getOutlet($outlet);
			if ($outlet_info) {
				$data['outlets'][] = array(
					'outlet_id' => $outlet_info['outlet_id'],
					'name'      => $outlet_info['name']
				);
			}
		}



    if(isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if(isset($this->error['error_name'])) {
      $data['error_name'] = $this->error['error_name'];
    } else {
      $data['error_name'] = '';
    }

    $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('wkpos/wkpos_receipt_form', $data));

  }

  public function delete() {
		$this->load->language('wkpos/wkpos_receipt');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('wkpos/wkpos_receipt');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $receipt_id) {
				$this->model_wkpos_wkpos_receipt->deleteReceipt($receipt_id);
			}

      $url = $this->getUrl();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('wkpos/wkpos_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->index();
  }

  protected function validateForm() {
    $this->registry->set('Htmlfilter', new Htmlfilter($this->registry));

		if (!$this->user->hasPermission('modify', 'wkpos/wkpos_receipt')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

    if ((utf8_strlen(trim($this->request->post['receipt_name'])) < 3) || (utf8_strlen(trim($this->request->post['receipt_name'])) > 64)) {
			$this->error['error_name'] = $this->language->get('error_name');
		}

    if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

    $this->request->post['content_header'] =  htmlentities($this->Htmlfilter->HTMLFilter(html_entity_decode($this->request->post['content_header']),'',true));

    $this->request->post['content_footer'] = htmlentities($this->Htmlfilter->HTMLFilter(html_entity_decode($this->request->post['content_footer']),'',true));

		return !$this->error;
  }


  protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'wkpos/wkpos_receipt')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

  public function autocomplete() {
    $json = array();
    $this->load->model('wkpos/wkpos_receipt');

    if (isset($this->request->get['filter_name'])) {
      $filter_name = $this->request->get['filter_name'];
    } else {
      $filter_name = '';
    }

    $filter_data = array(
     'filter_name' => $filter_name,
     'start'       => 0,
     'limit'       => 5,
    );

    $results = $this->model_wkpos_wkpos_receipt->getReceipts($filter_data);

    foreach ($results as $key => $result) {
      $json[$key]['receipt_id'] = $result['receipt_id'];
      $json[$key]['name'] = strip_tags(html_entity_decode($result['receipt_name'], ENT_QUOTES, 'UTF-8'));
    }
    $this->response->addHeader('Content-Type: application/json');
	  $this->response->setOutput(json_encode($json));
  }

  public function autocompleteOutlet() {

    $json = array();
    $this->load->model('wkpos/wkpos_receipt');

    if (isset($this->request->get['filter_name'])) {
      $filter_name = $this->request->get['filter_name'];
    } else {
      $filter_name = '';
    }

    $filter_data = array(
     'filter_name' => $filter_name,
     'start'       => 0,
     'limit'       => 5,
    );

    $results = $this->model_wkpos_wkpos_receipt->getOutlets($filter_data);

    foreach ($results as $key => $result) {
      $json[$key]['outlet_id'] = $result['outlet_id'];
      $json[$key]['name'] = strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'));
    }
    $this->response->addHeader('Content-Type: application/json');
	  $this->response->setOutput(json_encode($json));
  }

  public function printPreview() {
    if(!$this->config->get('module_wkpos_receipt_status') || !$this->config->get('module_wkpos_status')) {
      $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
    }
    $data = array();
    $this->load->model('wkpos/wkpos_receipt');
    $this->load->model('tool/image');
    $outlet_info = array();

    $data = array_merge($data , $this->load->language('wkpos/wkpos_receipt'));
    $url = $this->getUrl();

    if(isset($this->request->get['receipt_id'])) {
			$outlet_info = $this->model_wkpos_wkpos_receipt->getReceipt($this->request->get['receipt_id']);
		} else {
      $this->response->redirect($this->url->link('wkpos/wkpos_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true));
    }

    $data['location'] = str_replace('&amp;', '&', $this->url->link('wkpos/wkpos_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true));

    $data['cur_date'] = date("Y-m-d");
    $data['cur_time'] = date("h:i:s");

    if($outlet_info){
      $data['outletinfo'] = true;
      $data['show_store_name']    = $outlet_info['store_name'];
  		$data['show_store_address'] = $outlet_info['store_address'];
  		$data['show_order_date']    = $outlet_info['order_date'];
  		$data['show_order_time']    = $outlet_info['order_time'];
  		$data['show_order_id']      = $outlet_info['order_id'];
  		$data['show_cashier_name']  = $outlet_info['cashier_name'];
  		$data['show_customer_name'] = $outlet_info['customer_name'];
  		$data['show_shipping_mode'] = $outlet_info['shipping_mode'];
  		$data['show_payment_mode']  = $outlet_info['payment_mode'];
  		$data['show_store_logo']    = $outlet_info['store_logo'];
  		$data['paper_size']         = $outlet_info['print_size'];
  		$data['font_weight']        = $outlet_info['print_font_weight'];
  		$data['show_note']          = $outlet_info['order_note'];
  		$data['store_logo']         = $this->model_tool_image->resize($outlet_info['image'], 200, 50);
  		$data['show_header']        = $outlet_info['show_content_header'];
  		$data['show_footer']        = $outlet_info['show_content_footer'];

      $data['text_header']   = html_entity_decode($outlet_info['content_header'], ENT_QUOTES, 'UTF-8');
  		$data['text_footer']   = html_entity_decode($outlet_info['content_footer'], ENT_QUOTES, 'UTF-8');
  		$store_detail          = preg_replace('~\r?\n~', "\n", $outlet_info['extra_information']);
  		$data['store_detail']  = implode('<br>', explode("\n", ($store_detail)));
      $store_address         = preg_replace('~\r?\n~', "\n", $this->config->get('config_address'));
  		$data['store_address'] = implode('<br>', explode("\n", ($store_address)));
      $data['store_name']    = $this->config->get('config_name');
    } else {
      $data['outletinfo'] = false;
    }

		$this->response->setOutput($this->load->view('wkpos/wkpos_preview', $data));
  }
}
