<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/pos-waaneizadigitalmarket-com_master//admin/');
define('HTTP_CATALOG', 'http://localhost/pos-waaneizadigitalmarket-com_master//');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/pos-waaneizadigitalmarket-com_master//admin/');
define('HTTPS_CATALOG', 'http://localhost/pos-waaneizadigitalmarket-com_master//');

// DIR
define('DIR_APPLICATION', '/opt/lampp/htdocs/pos-waaneizadigitalmarket-com_master/admin/');
define('DIR_SYSTEM', '/opt/lampp/htdocs/pos-waaneizadigitalmarket-com_master/system/');
define('DIR_IMAGE', '/opt/lampp/htdocs/pos-waaneizadigitalmarket-com_master/image/');
define('DIR_STORAGE', '/opt/lampp/htdocs/pos-waaneizadigitalmarket-com_master//storage/');
define('DIR_CATALOG', '/opt/lampp/htdocs/pos-waaneizadigitalmarket-com_master/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'waaneiza_wwt2');
define('DB_PORT', '3306');
define('DB_PREFIX', 'ocart_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
