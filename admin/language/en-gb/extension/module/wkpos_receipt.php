<?php
// Heading
$_['heading_title']    = 'POS Receipt';

// Text
$_['text_extension']       = 'Extensions';
$_['text_success']         = 'Success: You have modified POS Receipt module!';
$_['text_edit']            = 'Edit POS Receipt Module';
$_['text_receipt']         = 'POS Receipt';
$_['text_add_receipt']     = 'Receipt';
$_['text_assign_receipt']  = 'Assign Receipt';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify POS Receipt module!';
